from django import forms

class HomeForm(forms.Form):
    loan_amount = forms.IntegerField()
    interest = forms.IntegerField()
    monthly_payment_amount = forms.IntegerField()

class LoansForm(forms.Form):
    loan_amount = forms.FloatField()
    interest_rate = forms.FloatField()
    number_of_months = forms.IntegerField()

class InvestForm(forms.Form):
    loan_balance = forms.FloatField()
    interest_rate = forms.FloatField()
    monthly_payment_amount = forms.FloatField()

class AnnuityForm(forms.Form):
    starting_principle = forms.FloatField()
    interest_rate = forms.FloatField()
    years_to_pay_out = forms.IntegerField()

class CompoundInterestForm(forms.Form):
    current_principle = forms.FloatField()
    annual_addition = forms.FloatField()
    effective_annual_rate = forms.FloatField()
    years_to_grow = forms.IntegerField()

class PresentValueForm(forms.Form):
    future_value = forms.FloatField()
    discounted_rate = forms.FloatField()
    years = forms.IntegerField()

class MortgageForm(forms.Form):
    loan_amount = forms.FloatField()
    mortgage_rate = forms.FloatField()
    year_to_pay = forms.IntegerField()



