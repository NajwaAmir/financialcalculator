# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic import TemplateView
from calculators.forms import HomeForm, LoansForm, InvestForm, AnnuityForm, CompoundInterestForm, MortgageForm, PresentValueForm

# Create your views here.

import math

# Test class
class HomeView(TemplateView):
    template_name = 'calculators/home.html'

    def get(self, request):
        form = HomeForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = HomeForm(request.POST)
        if form.is_valid():
            loan_amount = form.cleaned_data['loan_amount']
            interest = form.cleaned_data['interest']
            monthly_payment_amount = form.cleaned_data['monthly_payment_amount']
            total_months_needed = self.calculateRemainingMonths(loan_amount, interest, monthly_payment_amount)
            args = {'form': form, 'result': total_months_needed}
            return render(request, self.template_name, args)

        return render(request, self.template_name)

    def calculateRemainingMonths(self, loan_amount, interest, monthly_payment_amount):
        return loan_amount / monthly_payment_amount


class LoansView(TemplateView):
    template_name = 'calculators/loans.html'
    title = 'Loans'
    description = 'Determine the monthly payment amount for common loan types such as ' \
                  'student loans, personal loans, and auto loans.'

    def get(self, request):
        form = LoansForm()
        return render(request, self.template_name, {'form': form, 'title': self.title, 'description': self.description})

    def post(self, request):
        form = LoansForm(request.POST)
        print(form.errors)
        if form.is_valid():
            loan_amount = form.cleaned_data['loan_amount']
            interest_rate = form.cleaned_data['interest_rate']
            number_of_months = form.cleaned_data['number_of_months']
            response = self.calculateRemainingMonths(loan_amount, interest_rate, number_of_months)
            args = {'form': form, 'result': response, 'title': self.title, 'description': self.description}
            return render(request, self.template_name, args)

        return render(request, self.template_name)

    def calculateRemainingMonths(self, loan_amount, interest_rate, number_of_months):
        monthly_payments = 0
        total_interest = 0
        total_amount_to_be_repaid = 0
        ear = 0

        interest_rate = interest_rate/100
        monthly_rate = interest_rate/12

        monthly_payments = loan_amount/((1-((1/(1+monthly_rate))**number_of_months))/monthly_rate)
        total_interest = number_of_months*monthly_payments - loan_amount
        total_amount_to_be_repaid = total_interest + loan_amount
        ear = ((1+interest_rate/12)**12-1)*100

        loan_amount = '${:,.2f}'.format(loan_amount)
        monthly_payments = '${:,.2f}'.format(monthly_payments)
        total_interest = '${:,.2f}'.format(total_interest)
        total_amount_to_be_repaid = '${:,.2f}'.format(total_amount_to_be_repaid)
        ear = format(ear, '.2f')

        return {'loan_amount': loan_amount, 'monthly_payments': monthly_payments, 'total_interest': total_interest, 'ear': ear, 'total_amount_to_be_repaid': total_amount_to_be_repaid}


# TODO
class InvestView(TemplateView):
    template_name = 'TODO'
    title = 'Loan Payoff'

    def get(self, request):
        form = InvestForm()
        return render(request, self.template_name, {'form': form, 'title': self.title})

    def post(self, request):
        form = InvestForm(request.POST)
        if form.is_valid():
            loan_balance = form.cleaned_data['loan_balance']
            interest_rate = form.cleaned_data['interest_rate']
            monthly_payment_amount = form.cleaned_data['monthly_payment_amount']
            response = self.calculateRemainingMonths(loan_balance, interest_rate, monthly_payment_amount)
            args = {'form': form, 'result': response, 'title': self.title}
            return render(request, self.template_name, args)

        return render(request, self.template_name)

    def calculateRemainingMonths(self, loan_balance, interest_rate, monthly_payment_amount):
        months_to_payoff_debt = 0
        total_interest = 0
        total_to_pay_back = 0

        interest_rate = interest_rate / 100
        monthly_rate = interest_rate/12
        ear = ((1+monthly_rate)**12-1)*100

        numerator = 1 - ((loan_balance * monthly_rate)/monthly_payment_amount)
        denominator = 1/(1+monthly_rate)

        print(loan_balance * monthly_rate)
        print(((loan_balance * monthly_rate)/monthly_payment_amount))

        months_to_payoff_debt = math.log(numerator)/math.log(denominator)
        total_to_pay_back = monthly_payment_amount * months_to_payoff_debt
        total_interest = total_to_pay_back - loan_balance

        months_to_payoff_debt = int(math.ceil(months_to_payoff_debt))
        total_to_pay_back = '${:,.2f}'.format(total_to_pay_back)
        total_interest = '${:,.2f}'.format(total_interest)
        ear = format(ear, '.2f')

        return {'months_to_payoff_debt': months_to_payoff_debt, 'total_interest': total_interest, 'total_to_pay_back': total_to_pay_back, 'ear': ear}


class AnnuityView(TemplateView):
    template_name = 'calculators/annuity.html'
    title = 'Annuity'
    description = 'An annuity is a financial product that pays a fixed annual amount over a ' \
                  'given number of years, primarily used as an income stream for retirees. '

    def get(self, request):
        form = AnnuityForm()
        return render(request, self.template_name, {'form': form, 'title': self.title, 'description': self.description})

    def post(self, request):
        form = AnnuityForm(request.POST)
        if form.is_valid():
            starting_principle = form.cleaned_data['starting_principle']
            interest_rate = form.cleaned_data['interest_rate']
            years_to_pay_out = form.cleaned_data['years_to_pay_out']
            response = self.calculateAnnuity(starting_principle, interest_rate, years_to_pay_out)
            args = {'form': form, 'result': response, 'title': self.title, 'description': self.description}
            return render(request, self.template_name, args)

        return render(request, self.template_name)

    def calculateAnnuity(self, starting_principle, interest_rate, years_to_pay_out):
        annual_payout_amount = 0

        interest_rate = interest_rate/100

        v = 1/(1+interest_rate)
        denominator = (1-v**years_to_pay_out)/interest_rate

        annual_payout_amount = starting_principle/denominator
        annual_payout_amount = '${:,.2f}'.format(annual_payout_amount)

        return {'annual_payout_amount': annual_payout_amount}


class CompoundView(TemplateView):
    template_name = 'calculators/compoundinterest.html'
    title = 'Compound Interest'
    description = 'Compound interest is interest calculated on the initial principal and ' \
                 'on the accumulated interest of previous periods of a loan or deposit.'

    def get(self, request):
        form = CompoundInterestForm()
        return render(request, self.template_name, {'form': form, 'title': self.title, 'description': self.description})

    def post(self, request):
        form = CompoundInterestForm(request.POST)
        if form.is_valid():
            current_principle = form.cleaned_data['current_principle']
            annual_addition = form.cleaned_data['annual_addition']
            effective_annual_rate = form.cleaned_data['effective_annual_rate']
            years_to_grow = form.cleaned_data['years_to_grow']
            response = self.calculateCompound(current_principle, annual_addition, effective_annual_rate, years_to_grow)
            args = {'form': form, 'result': response, 'title': self.title, 'description': self.description}
            return render(request, self.template_name, args)

        return render(request, self.template_name)

    def calculateCompound(self, current_principle, annual_addition, effective_annual_rate, years_to_grow):
        future_value = 0
        interest_earned = 0

        effective_annual_rate = effective_annual_rate/100
        discounted_function = 1/(1+effective_annual_rate)**years_to_grow
        compounding_function = (1+effective_annual_rate)**years_to_grow
        annuity_function = (1 - discounted_function)/effective_annual_rate

        future_value = (current_principle * compounding_function) + annual_addition * annuity_function * compounding_function

        interest_earned = future_value - (current_principle + annual_addition * years_to_grow)

        future_value = '${:,.2f}'.format(future_value)
        interest_earned = '${:,.2f}'.format(interest_earned)

        return {'future_value': future_value, 'interest_earned': interest_earned}




class PresentValueView(TemplateView):
    template_name = 'calculators/presentvalue.html'
    title = 'Present Value'
    description = 'To obtain a specified balance in the future, the present value calculator helps ' \
                  'you find the amount you need to invest today to achieve that goal.'

    def get(self, request):
        form = PresentValueForm()
        return render(request, self.template_name, {'form': form, 'title': self.title, 'description': self.description})

    def post(self, request):
        form = PresentValueForm(request.POST)
        if form.is_valid():
            future_value = form.cleaned_data['future_value']
            years = form.cleaned_data['years']
            discounted_rate = form.cleaned_data['discounted_rate']
            response = self.calculatePresentValue(future_value, years, discounted_rate)
            args = {'form': form, 'result': response, 'title': self.title, 'description': self.description}
            return render(request, self.template_name, args)

        return render(request, self.template_name)

    def calculatePresentValue(self, future_value, years, discounted_rate):
        present_value = 0

        discounted_rate = discounted_rate/100

        denominator = (1 + discounted_rate)**years
        present_value = future_value/denominator
        present_value = '${:,.2f}'.format(present_value)

        return {'present_value': present_value}




class MortgageView(TemplateView):
    template_name = 'calculators/mortgage.html'
    title = 'Mortgage'
    description = 'This is a simple mortgage payment calculator to help you find the amount of your ' \
                  'monthly mortgage payments over a given number of years and mortgage rate. '

    def get(self, request):
        form = MortgageForm()
        return render(request, self.template_name, {'form': form, 'title': self.title, 'description': self.description})

    def post(self, request):
        form = MortgageForm(request.POST)
        if form.is_valid():
            loan_amount = form.cleaned_data['loan_amount']
            mortgage_rate = form.cleaned_data['mortgage_rate']
            year_to_pay = form.cleaned_data['year_to_pay']
            response = self.calculateMortgage(loan_amount, mortgage_rate, year_to_pay)
            args = {'form': form, 'result': response, 'title': self.title, 'description': self.description}
            return render(request, self.template_name, args)

        return render(request, self.template_name)

    def calculateMortgage(self, loan_amount, mortgage_rate, year_to_pay):
        monthly_payments = 0
        total_payments = 0
        total_interest = 0

        mortgage_rate = mortgage_rate/100
        monthly_rate = mortgage_rate/12
        number_of_months = year_to_pay * 12

        monthly_payments = loan_amount/((1-((1/(1+monthly_rate))**number_of_months))/monthly_rate)
        total_interest = number_of_months * monthly_payments - loan_amount
        total_payments = total_interest + loan_amount

        monthly_payments = '${:,.2f}'.format(monthly_payments)
        total_payments = '${:,.2f}'.format(total_payments)
        total_interest = '${:,.2f}'.format(total_interest)

        return {'monthly_payments': monthly_payments, 'total_payments': total_payments, 'total_interest': total_interest}


